# Parcial 2 de Inteligencia Artificial

Modelamiento [SVM](https://en.wikipedia.org/wiki/Support_vector_machine) para predicciones de delitos en la ciudad de Puerto Montt.

## Instrucciones de uso

Se puede previsualizar directamente desde este repositorio o ver el [Google Colab](https://drive.google.com/file/d/1tt37yXB7Wr1v4kbDK1rDpbffgR82JHUZ/view?usp=sharing).

### Profesor

Joel Sebastian Torres Carrasco ([@profeJoel](https://github.com/profeJoel))

### Integrantes

- Diego Muñoz ([@Awerito](https://gitlab.com/Awerito))
- Cristian Oyarzo ([@ClownSK](https://gitlab.com/ClownSk))
- Victor Rodriguez ([@Pejelagartoh](https://gitlab.com/Pejelagartoh))
- Sebastian Vidal ([@Ezfeik](https://gitlab.com/Ezfeik))
